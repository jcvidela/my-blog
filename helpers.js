import Link from "next/link";

export let linkElement = (url, desc) => (
 <Link href={url} passHref={true}>
   <a target="_blank" rel="noopener noreferrer" style={{textDecoration: 'none'}}>
     <strong style={{color: 'black'}}>{desc}</strong>
   </a>
 </Link>
);

export function getCurrentAge() {
  const dateString = "2000/03/28"
  let today = new Date();
  let birthDate = new Date(dateString);
  let age = today.getFullYear() - birthDate.getFullYear();
  let m = today.getMonth() - birthDate.getMonth();
  if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
      age--;
  }

  return age;
}