import React from "react";
import Head from "next/head";
import Layout from "../components/Layout.js";
import { fullName, siteTitle } from "../utils/constants.js";
import utilStyles from "../styles/utils.module.css";
import { useAnswers } from "../hooks";

import Grid from "../components/Grid";

export default function Home() {
  // hooks
  const answers = useAnswers();

  return (
    <Layout home>
      <Head>
        <title>{siteTitle}</title>
      </Head>

      <section
        className={utilStyles.alignHorizontally}
        style={{ textAlign: "center" }}
      >
        <p>
          Hello, I'm <strong>{fullName}</strong>, a Frontend Developer from
          Buenos Aires, Argentina.
        </p>
        <p>¿What would you like to know about me?</p>
      </section>

      <section>
        <Grid answers={answers} />
      </section>
    </Layout>
  );
}
